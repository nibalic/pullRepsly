# PullRepsly
**20.4.2018.**

15-min Google Script hack to get data on visits from Repsly API and save it in Google Sheets based on https://gist.github.com/varun-raj/5350595a730a62ca1954

1. Paste it and edit your data:

- Repsly API USER ID
- Repsly API Password
- Google Sheet ID

2. Setup triggers to run it once a day at least

Keep in mind that Repsly API is returing data in batches of 50 records.

Header row of the sheet should contain:
VisitID	TimeStamp	Date	RepresentativeCode	RepresentativeName	ExplicitCheckIn	DateAndTimeStart	DateAndTimeEnd	ClientCode	ClientName	StreetAddress	ZIP	ZIPExt	City	State	Country	Territory	LatitudeStart	LongitudeStart	LatitudeEnd	LongitudeEnd	PrecisionStart	PrecisionEnd	VisitStatusBySchedule	VisitEnded	lastTimeStamp

TODO:
- create header row first time
- fix script exit if there are no new records from the last run
- fix check is it a first run